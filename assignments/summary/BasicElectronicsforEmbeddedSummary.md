# Sensors and Acutators
* The building block of any embedded system is **Sensors** and **Acutators**

* Sensors-
    * They constitute to be the basic building block of any embedded system
    
    * They act as a connection between nature and microcontrollers or microprocessors
    
    * Their primary task is to convert any analog signals to digital signals or computer understandable format
    
    * They are more technically referred to as transducers
    
    * Example - A DHT11 temprature sensor it reads the temperature from the surrounding areas and the data pin outputs the corresponding temprature and humidity 

    ![sensors](https://www.electronicshub.org/wp-content/uploads/2017/11/Types-of-Sensors-Image-2.jpg)


* Working of DHT22 sensor
   
   
    ![working-sen](https://qph.fs.quoracdn.net/main-qimg-b985d63ffb25edb6432b812d6b3e9a2f)


* Acutators -
    
    * They are another form of transducers
    
    * They convert digital to analog signals ie they perform some tasks when recieve an electrical input

    * Their working is opposite to that of a sensor

    * Example - The automated car door locking mechanism where the car door locks when it recieves a signal from the key.
    
    ![acutator](https://i0.wp.com/3.212.229.64/wp-content/uploads/2020/04/dc_motors-1024x513.png?resize=771%2C386)

   
    ![sensor and acutator](https://bridgera.com//wp-content/uploads/2017/06/sensor_actuator_graphicssec1_pg16.jpg)

# Types of signals

* The major types of signals present
    * Analog signal
    * Digital signal
    * Periodic signal


| Analog | Digital |
| ------ | ------ |
| Continous Signals| Discrete Signals |
| They have values at all time intervals| They have values only at integral time values |
| Denoted by sine waves| Denoted by square waves|
| It has a continous range of values to represent information| It has only Discrete values to represent information|
| They have values at all time intervals| They have values only at integral time values |
| Denoted by sine waves| Denoted by Square Waves |
| Analog singals are suited for audio and video transmission|Digital Signals are suited for computing and digital electronics|

![signals](https://1.bp.blogspot.com/-BOJ9ZDChtnQ/Xcw3rI-uRII/AAAAAAAACUM/lNFGx00PNucfiRQewbFUqjhz8g_cY2ingCLcBGAsYHQ/s1600/Difference%2Bbetween%2BAnalog%2Band%2BDigital%2BSignal.png)

# Microprocessors and Microcontrollers

* Microprocessors are general purpose chips
* Microcontrollers are specific purpose devices

| Microprocessors | Microcontrollers |
| ------ | ------ |
| It is the heart of Computer system | It is the heart of an embedded system |
| Consists of only a Central Processing Unit| Contains a CPU, Memory, I/O all integrated into one chip |
| Uses an external bus to interface to RAM, ROM, and other peripherals| Uses an internal controlling bus |
| Based on Von Neumann architecture| Based on Harvard architecture|
| It's complex and expensive, with a large number of instructions to process.| It's simple and inexpensive with less number of instructions to process.|
| It has a smaller number of registers, so more operations are memory-based| It has more register. Hence the programs are easier to write. |

![archi](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)

#  Raspberry pi

* Raspberry pi is a linux based on chip computer

* An external memory card has to be used for memory 

* Uses an ARM architecture

* Features - 
    * CPU: Quad-core 64-bit ARM Cortex A53 clocked at 1.2 GHz
    * Clock: 400MHz
    * RAM: 1GB 
    * USB ports: 4
    * Peripherals: 17 GPIO plus specific functions, and HAT ID bus
    * Bluetooth: 4.1
    * Power source: 5 V 
    * Network: 10/100Mbps Ethernet and 802.11n Wireless LAN

* Raspberry PI has various interfaces - 
    * GPIO
    * UART
    * SPI
    * I2C

    ![rpi](https://i.ytimg.com/vi/6nL2a3wNTPE/maxresdefault.jpg)

# Communication

* The Communication inside computers take place in two ways -
    * Serial
    * Parallel

|Basis | Serial | Parallel |
|-----| ------ | ------ |
|Data transmission speed|Slow|Comparatively fast|
|Suitable for| Long Distance|Short Distance|
|High frequency operation| More Efficient | less Efficient|
|Number of transmitted bit/clock cycle| Only one bit| Many bits|
|Examples|UART,SPI,I2C|GPIO|



