# Safety Instructions 

* While working with **Power Supplies** following should be taken care -
    * Avoid contact with energized electrical circuits
    * Disconnect the power source before servicing or repairing electrical equipment.
    * Use only tools and equipment with non-conducting handles when working on electrical devices. 
    * Minimize the use of electrical equipment in cold rooms 
    * Enclose all electric contacts and conductors so that no one can accidentally come into contact with them
    * Do not store highly flammable liquids near electrical equipment
    * Do not rely on grounding to mask a defective circuit nor attempt to correct a fault by insertion of another fuse or breaker, particularly one of larger capacity
    * Never use metallic pencils or rulers, or wear rings or metal watchbands when working with electrical equipment


* While working with **GPIO** -
    
    * Not to plug components in while the GPIO is running
    * Not to plug anything with a high (or negative) voltage - the GPIOs are binary and consider 3.3 as "on" as anything higher will damage the controller.
    * Always connect low voltage components with suitable resistors to prevent damage of the components
    * High voltage components should have a transistor to drive it

* Using ** UART** -
    * The Tx pin of device 1 should be connected to Rx pin of device 2 and Rx pin of device 1 should be Tx pin of device 2
    * Protocol requires one start and stop bit
    * Baud rate must be the same on both devices

![uart](https://www.electronicshub.org/wp-content/uploads/2017/06/UART-Working.jpg)

* Using **SPI**
    * It uses seprate data lines for data and clock
    * Slave should be active to recieve and send data

![spi](https://cdn.sparkfun.com/assets/d/6/b/f/9/52ddb2d8ce395fad638b4567.png)

* Using **I2C**-
    
    * Usage of pull up resistors is mandatory to set up the communication Protocol
    * Volatge between two systems must not be significant
    * Masters data rate should not exceed the slaves ability to provide data

![i2c](https://cdn.sparkfun.com/assets/3/d/1/b/6/51adfda8ce395f151b000000.png) 