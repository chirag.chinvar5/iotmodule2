# IIoT Protocols

## 4-20ma Current Loop

* A 4-20ma Protocol is a widely used Protocols

* It takes the sensor data and then further converts it to a current range between 4 and 20ma

* It has widely replaced 0-10v protocol because -
   
    * Voltage drop - When long cables are used there are chances of a huge potential drop which affects the output of the transducer ,this effect can be overcome by 4-20ma method
    * Electromagnetic interference - Voltages are more prone to EMI 
    * Detecting an open circuit - If the meter reads 0v it could be possible that the circuit is grounded or there might be a wire cut,but in the case of 4-20ma ,4ma is the minimum but if 0ma is read then it as open circuit


* Working -

    * To understand the working we should consider OHM's law where Voltage=current x Resistance (v=IR)
    * Consider a circuit with n resistors in series is connected 
    * To drive the electrons around the circuit a certain power supply of known value is connected 
    * From basics of OHM's law it is clear that the current in a series circuit is constant across any resistor(load)
    * There is a voltage drop across each element in the circuit as voltage divides in a series circuit
    * As the value of current is constant it is taken as a parameter to measure the values of sensors.
    * Below image justifes the same 

![ohm](https://www.allaboutcircuits.com/uploads/articles/component-voltages-calculation.jpg)

* Basic Components -
    * Power supply 
        * Power supplies for 2-wire transmitters must always be DC because the change in current flow represents the parameter that 
is being measured. If AC power were used, the current in the loop would be changing all the time.

        * Current loops using 3-wire transmitters can have either AC or DC power supplies.  The most common AC power supply is the 24 VAC control transformer or 230A Ac.
    
    * Transmitter 
        * The transmitter converts the real world signal, such as flow, speed, position, level, temperature, humidity, pressure, etc., into the control signal necessary to regulate the flow of current in the current loop. The level of loop current is adjusted by the transmitter to be proportional to the actual sensor input signal.
        
        * Generally the power to transmitters as a range, 12 to 36 VDC The lower voltage is the minimum voltage necessary to guarantee proper transmitter operation.  The higher voltage is the maximum voltage the transmitter can withstand and operate to its stated specifications

    * Reciever resistor
        * It is much easier to measure a voltage than it is to measure a current.  Therefore, many current loop circuits (such as the circuit in Figure 1) use a Receiver Resistor (Rreceiver) to convert the current into a voltage

    * Wire 
        * The impact of the wire resistance in the current loop is often ignored, as it usually contributes negligible voltage drop over short distances and small installations.

        * However, over long transmission distances, this drop can be significant and must be accounted for, as some current loop installations will use wire distributed over hundreds and even thousands of feet.

* Working of 4-20ma current loop is shown below-

![block](https://cdn.instrumentationtools.com/wp-content/uploads/2016/09/instrumentationtools.com_how-a-4-20ma-transmitter-works-3.gif)

* Advantages -
    * Simplest option to connect and configure
    * Less wirirng requried
    * Better for long distances

* Disadvantages -
    * It processes only one kind of signal
    * Need for isolation equipments increases.


## ModBus

* ModBus is a communication protocol for Logic Controllers developed by modicon
* It works on the method of master and slave,where there is one master and many slaves(transducers etc.)
* It has a 28 bit start and stop signal
* The function code is used to send data from master to slave
* ModBus has two interfaces -
    * ModBus RTU -RS485,RS232
    * ModBus TCP/IP- Ethernet

![modbus](https://realpars.com/wp-content/uploads/2018/12/What-is-Modbus-and-How-it-Works.jpg)

### ModBus RTU
* ModBus RTU  is a communication protocol that can happen only over within a certain fixed distance ie 15m.
* ModBus RTU comes with different interfaces like RS232,RS422,RS485
* RS232 is a interface which is not widely used becuase of its limitiation of only one slave and master 
* In its place RS485 and RS422 is used because we can have many slaves

##### RS485 
* RS485 is communication interface that supports many slaves
* Its basic configuration consists of 4 pins
* Can host upto 32 slaves without a repeater and 247 slaves with repeaters to a maximum distance of 1500m
* As asynchronous all slaves must be on same baud rate as master
    * RX+
    * RX-
    * TX+
    * TX-
    * GND 

![rs485](https://www.ozeki.hu/attachments/5851/RS485_master-slave_half_duplex.gif)


## OPCUA

* OPCUA is an open source industrial standard for information interchange
* It invloves data transfer between central systems and various transducer etc
* It is supported by many OS's
* It can be used in a closed network or via the cloud
* It is based on a client server model
* It is scalable
* The architecture of a UA application, independent of whether it is the server or client part, is structured into levels.
* UA Security consists of authentication and authorization, encryption and data integrity via signatures

![opcua](https://www.renesas.com/in/en/img/solutions/industrial-automation/industrial-network/industrial-ethernet-and-fieldbus/opc-ua/block.jpg)


# Cloud Protocols

## MQTT(Message Queuing Telemetry Transport)

* MQTT is a lightweight messaging protocol for IoT applications based on TCP
* It was made for devices with less bandwidth and constraint devices
* It works on the publish subscribe model
* To recieve messages and publish messages to a particular devices they should be subscribed to a **TOPIC**
* For effective communication a broker a very common example is a mosquitto

* Working
Each client can both produce and receive data by both publishing and subscribing, i.e. the devices can publish sensor data and still be able to receive the configuration information or control commands.This helps in both sharing data, managing and controlling devices.With MQTT broker architecture, the devices and application becomes decoupled and more secure. MQTT uses Transport Layer Security (TLS) encryption with user name, password protected connections, and optional certifications that requires clients to provide a certificate file that matches with the server’s. The clients are unaware of each others' IP address.

![mqtt](https://www.electronicwings.com/public/images/user_images/images/NodeMCU/NodeMCU%20Basics%20using%20Arduino%20IDE/NodeMCU%20MQTT%20Client/MQTT%20Broker%20nw.png)



## HTTP 

* HTTP is hypertext transfer protocol which is another protocol to transport messages over the web
* HTTP functions as a request-response protocol in the client-server computing model
* An HTTP request is made by a client, to a named host, which is located on a server. The aim of the request is to access a resource on the server
* An HTTP request consists of 
    * The request line - The server could be asked to send the resource to the client.
    * HTTP headers- These are written on a message to provide the recipient with information about the message, the sender, and the way in which the sender wants to communicate with the recipient
    * Body - Content of any HTTP message can be referred to as a message body.


* Working

The client submits an HTTP request message to the server. The server, which provides resources such as HTML files and other content, or performs other functions on behalf of the client, returns a response message to the client. The response contains completion status information about the request and may also contain requested content in its message body

![http](https://upload.wikimedia.org/wikipedia/commons/2/2d/Prj10.jpeg)